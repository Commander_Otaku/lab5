# README #


### What is this repository for? ###

* This program displays clock information.
* Version 1
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* The set up was straightforward. Just needed to add a clock repo.
* Configuration
* Dependencies
* Database configuration
* How to run tests
* To deploy or make changes run "git pull" to make sure you are up to date.
* Then edit the source code to your specifications. After you have made changes
* run "git add" and "commit" to add your new file to the repository. Finally, flush
* your file out to bitbucket by using the command "push."

### Contribution guidelines ###

* Writing tests - Nick Glyder
* Code review - Nick Glyder
* Other guidelines - Nick Glyder

### Who do I talk to? ###

* Repo owner: Austin Alexander Briley
* Clemson University CpSc department.